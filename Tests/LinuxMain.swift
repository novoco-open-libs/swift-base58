import XCTest

import SwiftBase58Tests

var tests = [XCTestCaseEntry]()
tests += SwiftBase58Tests.allTests()
XCTMain(tests)
